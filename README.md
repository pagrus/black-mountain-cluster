# black-mountain-cluster
a clusterer for kfjc shows

## TL;DR

### DO NOT USE
This should be considered pre-alpha and in no way ready for normal use

At the very least don't re-scrape the playlists, I can give you a zip (1.6GB, 56938 HTML files)

BMC attempts to collect shows broadcast on KFJC into clusters or categories based solely on the artist, title, and album text.

It is my hope that then new listeners are more easily able to find shows to listen to, and that new DJs can find audiences more quickly.

## Process
1. scrape show info pages
2. extract song/artist/album info
3. insert into DB
4. cluster into groups
5. ???

Depending on the outcome of steps 1-4 steps 5 and above might be any number of things:
- make predictions for new DJs
- update continuously 
- or at set intervals
- recommend shows or DJs to new listeners
- PCA, plot top two components to evaluate cluster quality
- and so forth. 

Let's not get ahead of ourselves though. An earlier version of this put everything into an S3 bucket right off the bat for example

## Components

### scraper
Gets HTML from KFJC website, puts it in a dir
- takes range as arg
- checks for dupes before downloading
- sleep/delay: to be nice?
- verify server response code

### extractor
Pulls structured data out of HTML, inserts into db
- insert/append vs rebuild
- sqlite seems to be fine for 50000 rows, do we need postgres?

### clusterer
Decide on number of clusters, stop words, etc
- try a bunch
- label with dj info? time of day? day of week?
- plot that shit
