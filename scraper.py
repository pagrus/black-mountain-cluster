#!/usr/bin/python3

import requests
import os
import time
import random
import requests
from bs4 import BeautifulSoup

'''

scrapes html from kfjc website

for testing, maybe get 10 random pages at a time

if playlist page is cached then get the max_id from that,
otherwise get a fresh copy and determine max_id



'''

def get_curr_playlist_html():
    url = "https://kfjc.org/listen/playlists"
    pl_req = requests.get(url)
    if pl_req.status_code == 200:
        filepath = "curr_playlists.html"
        with open(filepath, "w") as pl_file:
            pl_file.write(pl_req.text)

def get_cur_playlists():
    pl_path = "curr_playlists.html"
    with open(pl_path, 'r') as fh:
        pl_data=fh.read()
    pl_soup = BeautifulSoup(pl_data, 'html.parser')
    anchors = pl_soup.find_all('a')
    playlists = set()
    for anchor in anchors:
        if anchor['href'][:19] == '/listen/playlist?i=':
            playlists.add(int(anchor['href'][19:]))
    return playlists

def get_playlist(playlist_id, base_path):
    
    headers = {
    'User-Agent': 'black mountain clusterer project v0.3 https://gitlab.com/pagrus/black-mountain-cluster',
    'From': 'kfjc@nebulos.us'
    }
    id_str = str(playlist_id)
    url = 'https://kfjc.org/listen/playlist?i=' + id_str
    pl_req = requests.get(url, headers)
    if pl_req.status_code == 200:
        req_size = int(pl_req.headers.get('Content-Length'))
        if req_size < warning_threshold:
            print("warning! content-length for pid {} is {}".format(id_str, req_size))
        print("got playlist id {}, {} bytes".format(id_str, req_size))
        filepath = base_path + id_str + ".html"
        with open(filepath, "w") as pl_file:
            pl_file.write(pl_req.text)
    else:
        print("status code is {}, skipping id {}".format(pl_req.status_code, id_str))
    return(pl_req.status_code)
    
def get_skip_list():
    sl_path = "skips.txt"
    skips = set()
    with open(sl_path, 'r') as fh:
        for skip in fh:
            skips.add(int(skip))
    return skips
    
get_curr_playlist_html()
last_playlist = max(get_cur_playlists())

scrape_dir = 'html/'
scraped_set = set(map((lambda x: int(x[0:-5])), os.listdir(scrape_dir)))

wait_secs = 2
# max_id = 57222
max_id = last_playlist
# batch_size = 20000
# batch_size = max_id - len(scraped_set)
warning_threshold = 1000     

range_set = set(range(1, max_id))
skip_set = get_skip_list()
to_scrape_set = (range_set - scraped_set) - skip_set
print("{} playlists found in {}, attempting to retrieve {} remaining files".format(len(scraped_set), scrape_dir, len(to_scrape_set)))

for pid in to_scrape_set:
    status_code = get_playlist(pid, scrape_dir)
    if status_code != 200:
        with open("skips.txt", 'a') as fh:
            fh.write(str(pid) + '\n')
    time.sleep(wait_secs)

